﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.Elementos;

namespace Projeto_Estoque.Controllers
{
    public class CadastroUsuarioController : Controller
    {
      
        public IActionResult Cadastro(Usuario novoUsuario)
        {
            return View("\\Views\\Home\\CadastroUsuario.cshtml");
        }

    
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
