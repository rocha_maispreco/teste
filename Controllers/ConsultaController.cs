﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Validações;

namespace Projeto_Estoque.Controllers
{
    public class ConsultaController : Controller
    {
        //public IActionResult ConsultaProduto()
        //{
        //    return View("\\Views\\Home\\Consulta.cshtml");
        //}

        public IActionResult ConsultaProduto(Produto produto)
        {
            if(produto.Nome != null)
                ViewData["Message"] = produto.Nome;
            else
                ViewData["Message"] = "Não funcionou";

            TipoPerfil tipoPerfil = LoginValidacao.ValidaPerfil();

            if (tipoPerfil == TipoPerfil.Usuario || tipoPerfil == TipoPerfil.Administrador)
            {
                return View("\\Views\\Home\\Consulta.cshtml", produto);               
            }
            else
            {
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }
                
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
