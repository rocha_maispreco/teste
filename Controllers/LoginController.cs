﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Validações;

namespace Projeto_Estoque.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Login(Usuario usuario)
        {     
                return View("\\Views\\Home\\Login.cshtml");
        }

        public IActionResult Logar(Usuario usuario)
        {

            if (!LoginValidacao.ValidaLogin(usuario))
            {
                ModelState.AddModelError("Email", "Email ou senha estão incorretos");
                ModelState.AddModelError("Senha", "Email ou senha estão incorretos");
            }
            LoginValidacao.CarregaTipoPerfil(usuario);
            if (ModelState.IsValid)
            {
                return View("\\Views\\Home\\Index.cshtml");
            }
            else
            {

                return View("\\Views\\Home\\Login.cshtml");
            }
        }
        public IActionResult Sair()
        {
            LoginValidacao.SairSistema();
            return Login(Usuario.GetInstance());
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
