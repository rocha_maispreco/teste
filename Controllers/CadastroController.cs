﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Projeto_Estoque.Elementos;
using Projeto_Estoque.Validações;

namespace Projeto_Estoque.Controllers
{
    public class CadastroController : Controller
    {
        public IActionResult Cadastro()
        {
            TipoPerfil tipoPerfil = LoginValidacao.ValidaPerfil();
            if (tipoPerfil == TipoPerfil.Administrador)
                return View("\\Views\\Home\\Cadastro.cshtml");
            else if (tipoPerfil == TipoPerfil.Usuario)
                return View("\\Views\\Home\\ErroAutorizacao.cshtml");
            else
            {   
                Usuario.DisconectarUsuario();
                return View("\\Views\\Home\\Login.cshtml");
            }

        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
