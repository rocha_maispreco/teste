﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projeto_Estoque.Controllers;
using Projeto_Estoque.Elementos;

namespace Projeto_Estoque.Validações
{
    public abstract class LoginValidacao
{
        public static bool ValidaLogin(Usuario usuario)
        {           
            if(usuario?.email?.Length > 5)            
                return true;
            if (usuario?.Senha?.Length > 5)
                return true;
            return false;
        }
        public static void CarregaTipoPerfil(Usuario usuario)
        {
            usuario.TipoPerfil = TipoPerfil.Usuario;
            if (usuario?.email == "123456")
                usuario.TipoPerfil = TipoPerfil.Administrador;
            if (usuario?.email == "654321")
                usuario.TipoPerfil = TipoPerfil.Usuario;

            Usuario.SetInstance(usuario);         
        }

        public static void SairSistema()
        {
            Usuario usuario = Usuario.GetInstance();
            usuario.TipoPerfil = TipoPerfil.NaoConectado;
            Usuario.SetInstance(usuario);
        }
        public static TipoPerfil ValidaPerfil()
        {
            Usuario usuario = Usuario.GetInstance();

            return usuario.TipoPerfil;

        }
    }

}