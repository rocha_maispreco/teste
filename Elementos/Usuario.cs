using System;

namespace Projeto_Estoque.Elementos
{
    public class Usuario
    {
        private static Usuario _instance;
        public static Usuario GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Usuario();
                _instance.TipoPerfil = TipoPerfil.NaoConectado;
            }
            return _instance;
        }
        public static void SetInstance(Usuario usuario)
        {
            _instance = usuario;
        }
        public static void DisconectarUsuario()
        {
            _instance.TipoPerfil = TipoPerfil.NaoConectado;
        }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Senha { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
        public TipoPerfil TipoPerfil { get; set; }




}
}