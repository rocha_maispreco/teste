using System;

namespace Projeto_Estoque.Elementos
{
    public class Produto
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Codigo { get; set; }
        public float Valor { get; set; }
        public float Categoria { get; set; }


    }
}